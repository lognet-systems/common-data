
# Dependency configuration

Snapshots:

```
repositories {
	maven { url 'http://oss.jfrog.org/artifactory/oss-snapshot-local' }
}

dependencies {
	implementation 'com.lognet.red.common:common-data:1.0.1-SNAPSHOT'
}
```

Releases:

```
repositories {
	jcenter()
}

dependencies {
	implementation 'com.lognet.red.common:common-data:1.0.1'
}
```