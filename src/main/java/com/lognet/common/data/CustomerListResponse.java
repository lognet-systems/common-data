package com.lognet.common.data;

import lombok.Data;

import java.util.List;

/**
 * Created by michaelz on 09/06/2020.
 */
@Data
public class CustomerListResponse {
    private List<CustomerDto> customers;
}
