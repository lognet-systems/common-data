package com.lognet.common.data;

import com.lognet.common.type.GenderType;
import com.lognet.common.type.PhoneType;
import com.lognet.common.type.TitleType;
import lombok.*;

import java.util.List;

/**
 * Created by michaelz on 09/06/2020.
 */
@Data
public class CustomerDto {
    private String extId;
    private String fullName;
    private TitleType title;
    private String nameRef;
    private List<String> emails;
    private String birthDate;
    private GenderType gender;
    private String mealCode;
    private Address customerAddress;
    private List<Phone> phones;
    private List<Passport> passports;
    private List<FrequentFlyer> frequentFlyers;
    private String clerkName; //Added in version 1.0.4


    @Data
    public static class Address {
        private String country;
        private String city;
        private String street;
        private String zipCode;
        private String state;
        private String latitude;
        private String longitude;
    }
    @Data
    public static class Phone {
        private String prefix;
        private String suffix;
        private PhoneType phoneType;
    }
    @Data
    public static class Passport {
        private String nationality;
        private String passportNumber;
        private String expirationDate;
        private String issueDate;
        private String issueCountry;
    }
    @Data
    public static class FrequentFlyer {
        private String vendorCode;
        private String number;
    }
}