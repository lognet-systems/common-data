package com.lognet.common.error;


import lombok.*;

/**
 * Created by michaelz on 09/06/2020.
 */
@Builder
@ToString
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorInfo {
    private String code;
    private String description;
    private String rootCause;
}
