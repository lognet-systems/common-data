package com.lognet.common.type;

import lombok.Getter;

/**
 * Created by michaelz on 09/10/2020.
 */
public enum GenderType {
    M("MALE"),
    F("FEMALE"),
    MI("MALE_INFANT"),
    FI("FEMALE_INFANT"),
    U("UNKNOWN_AT_THE_TIME_OF_BOOKING"),
    X("UNSPECIFIED");

    @Getter
    private String description;

    GenderType(String description) {
        this.description = description;
    }
}
