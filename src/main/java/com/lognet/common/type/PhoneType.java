package com.lognet.common.type;

import lombok.Getter;

/**
 * Created by michaelz on 09/10/2020.
 */
public enum PhoneType {
    H("HOME"),
    B("BUSINESS"),
    C("CELL"),
    M("MOBILE"),
    F("FAX"),
    A("AGENCY"),
    HTL("HOTEL");

    @Getter
    private String description;

    PhoneType(String description) {
        this.description = description;
    }
}
