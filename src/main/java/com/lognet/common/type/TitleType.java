package com.lognet.common.type;

/**
 * Created by michaelz on 09/10/2020.
 */
public enum TitleType {
    MR, MRS, MS, DR, MISS, MSTR, MLLE, SIR, FATHER, SISTER, BROTHER,
    REVEREND, LT, CAPT, CONGRESSMAN, PROF, DUKE, DUCHESS
}
